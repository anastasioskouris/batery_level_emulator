# Battery Level Emulator

A simple application that emulates battery level as a sinus function to time.

An HTTP endpoint also exposes "metrics" for scrapping.

## Requirements

- support for C++11
- `libboost-system`
- `libcppnetlib-dev`
- `libjsoncpp-dev`

Install with:

```
sudo apt-get install libboost-system \
                     libcppnetlib-dev \
                     libjsoncpp-dev
```

## Building and Installation

```
mkdir build
cd build
cmake ../
make
mkdir _deploy/
make DESTDIR=./_deploy/ install
```

This will install the `dummy-device` binary and the `scrapper.sh` script for
scrapping metrics to the provided HTTP endpoint URL.

## Usage

```
./_deploy/usr/local/bin/dummy-device [address [port]]
```

The default `address` is `0.0.0.0`(any address) and the default `port` is
`8080`.

### Example

#### Start dummy-device

```
./_deploy/usr/local/bin/dummy-device
```

This starts the dummy-device and lets it expose its metrics to `tcp *:8080`:

#### Get metrics

Hit [http://127.0.0.1:8080/](http://127.0.0.1:8080/) to get metrics.


Or execute:

```
curl http://127.0.0.1:8080 2>/dev/null | jq -r ""
```
Or execute:

```
./_deploy/usr/local/bin/scrapper.sh http://127.0.0.1:8080/
```

The output will be something like this:

```
{
  "metrics": [
    {
      "battery_level": 83.37316610328097
    }
  ]
}
```
