#ifndef BATTERY_LEVEL_EMULATOR_DEVICE_BATTERY_HPP
#define BATTERY_LEVEL_EMULATOR_DEVICE_BATTERY_HPP

#include <mutex>
#include <thread>

#include <jsoncpp/json/json.h>

namespace BatteryLevelEmulator {
namespace Device {

class Battery {
 public:
  /**
   * Emulate a battery with initial_level, max_level, increment_rate and
   * increment_interval.
   */
  Battery(double initial_level, double max_level, double increment_rate,
          std::chrono::milliseconds increment_interval);
  ~Battery();

  double getLevel();
  Json::Value getMetrics();
  void kill();

 private:
  void doWork();
  void setLevel(double level);

  std::mutex level_mutex_;
  double level_ = 0.0;
  const double max_level_ = 100.0;
  const double increment_rate_ = 0.001;
  const std::chrono::milliseconds increment_interval_ =
      static_cast<std::chrono::milliseconds>(100);
  std::thread worker_;
  std::mutex stop_mutex_;
  bool stop_ = false;
};
}
}

#endif  // BATTERY_LEVEL_EMULATOR_DEVICE_BATTERY_HPP
