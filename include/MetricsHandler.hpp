#ifndef BATTERY_LEVEL_EMULATOR_SERVER_METRICS_HANDLER_HPP
#define BATTERY_LEVEL_EMULATOR_SERVER_METRICS_HANDLER_HPP

#include <functional>
#include <vector>

#include <jsoncpp/json/json.h>
#include <boost/network/protocol/http/server.hpp>

namespace BatteryLevelEmulator {
namespace Server {

class MetricsHandler;
typedef boost::network::http::server<MetricsHandler> MetricsServer;

class MetricsHandler {
 public:
  void addMetricsCallback(const std::function<Json::Value()> &cb);
  void log(MetricsServer::string_type const &info);
  void operator()(MetricsServer::request const &request,
                  MetricsServer::response &response);

 private:
  std::vector<std::function<Json::Value()>> metrics_cbs_;
};
}
}

#endif  // BATTERY_LEVEL_EMULATOR_SERVER_METRICS_HANDLER_HPP
