#!/bin/bash

# script for scrapping battery-level-emulator metrics

if [ -z "$1" ]; then
	(>&2 echo "no scraping url provided!")
	echo
	echo "Usage: $0 <HTTP-URL>"
	echo "  e.g. $0 http://127.0.0.1:8080/"
	echo
	exit 1;
fi
SCRAPPING_URL="$1"

if ! command -v curl >/dev/null 2>&1; then
	(>&2 echo "curl command not found!")
	exit 2;
fi

OUTPUT=$(curl -XGET $SCRAPPING_URL 2>/dev/null)
err_code=$?
if [ $err_code -gt 0 ] ; then
	(>&2 echo "curl command failed!")
	exit $err_code;
fi
echo "$OUTPUT"
