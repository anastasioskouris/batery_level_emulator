#include <chrono>
#include <cmath>

#include "Battery.hpp"

#define PI 3.14159265

namespace BatteryLevelEmulator {
namespace Device {

Battery::Battery(double initial_level, double max_level, double increment_rate,
                 std::chrono::milliseconds increment_interval)
    : level_(initial_level),
      max_level_(max_level),
      increment_rate_(increment_rate),
      increment_interval_(increment_interval),
      worker_(&Device::Battery::doWork, this) {}

Battery::~Battery() {
  try {
    kill();
  } catch (const std::system_error &) {
  }
}

void Battery::doWork() {
  for (double radiants = 0.0;;
       radiants = (radiants > PI) ? 0.0 : (radiants + PI * increment_rate_)) {
    {
      std::lock_guard<std::mutex> lk(stop_mutex_);
      if (stop_) break;
    }

    setLevel(sin(radiants) * max_level_);
    std::this_thread::sleep_for(increment_interval_);
  }
}

void Battery::setLevel(double level) {
  std::lock_guard<std::mutex> lk(level_mutex_);
  level_ = level;
}

double Battery::getLevel() {
  std::lock_guard<std::mutex> lk(level_mutex_);
  return level_;
}

Json::Value Battery::getMetrics() {
  Json::Value root;
  root["battery_level"] = getLevel();
  return root;
}

void Battery::kill() {
  {
    std::lock_guard<std::mutex> lk(stop_mutex_);
    stop_ = true;
  }
  if (worker_.joinable()) worker_.join();
}
}
}
