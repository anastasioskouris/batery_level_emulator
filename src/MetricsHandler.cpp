#include <iostream>

#include "MetricsHandler.hpp"

namespace BatteryLevelEmulator {
namespace Server {

void MetricsHandler::operator()(MetricsServer::request const &request,
                                MetricsServer::response &response) {
  Json::FastWriter fastWriter;
  Json::Value root;
  Json::Value metricsList;
  for (auto metric_cb: metrics_cbs_) {
    metricsList.append(metric_cb());
  }

  root["metrics"] = metricsList;
  response = MetricsServer::response::stock_reply(MetricsServer::response::ok,
                                                  fastWriter.write(root));
}

void MetricsHandler::log(MetricsServer::string_type const &info) {
  std::cerr << "ERROR: " << info << std::endl;
}

void MetricsHandler::addMetricsCallback(
    const std::function<Json::Value()> &cb) {
  metrics_cbs_.push_back(cb);
}
}
}
