#include <functional>
#include <iostream>
#include <string>

#include "Battery.hpp"
#include "MetricsHandler.hpp"

using namespace BatteryLevelEmulator;
using namespace BatteryLevelEmulator::Device;
using namespace BatteryLevelEmulator::Server;

int main(int argc, char **argv) {
  std::string address;
  std::string port;

  switch (argc) {
    case 3:
      port = argv[2];
      address = argv[1];
      break;
    case 2:
      port = "8080";
      address = argv[1];
      break;
    case 1:
      port = "8080";
      address = "0.0.0.0";
      break;
    default:
      std::cerr << "Usage: " << argv[0] << " [address [port]]" << std::endl;
      return 1;
  }

  // Initialize devices
  Battery b(0.0, 100.0, 0.00001, static_cast<std::chrono::milliseconds>(1));

  MetricsHandler handler;
  MetricsServer::options options(handler);

  // add device callbacks to HttpHandler
  handler.addMetricsCallback(std::bind(&Battery::getMetrics, &b));

  // Serve metrics
  try {
    MetricsServer server_(options.address(address).port(port));
    server_.run();
  } catch (std::exception &e) {
    std::cerr << "Error while running server: " << e.what() << std::endl;
    return 1;
  }

  return 0;
}
